# Mergin

Despliegue para [Mergin](https://github.com/lutraconsulting/mergin), plataforma web para almacenamiento y sincronización de proyectos con datos georeferenciados para múltiples usuarios y dispositivos.

La primera vez que se despliegue el servicio es necesario inicializar la base de datos de usuarios y crear un usuario administrador

```
docker exec -it input_mergin... flask init-db
docker exec -it input_mergin... flask add-user admin changeme --is-admin --email admin@change.me
```

También es necesario establecer permisos al directorio de trabajo

```
sudo chown -R  901:999 ./data/
```